import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './infrastructure/auth/auth.guard';
import { AuthService } from './infrastructure/auth/auth.service';
import { LoginComponent } from './login/login.component';
import { AppDialogComponent, AppDialogContent } from './shared/app-dialog/app-dialog.component';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    AppDialogComponent,
    AppDialogContent,
  ],
  entryComponents: [AppDialogComponent, AppDialogContent],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    AuthService,
    AuthGuard,
  ],
})
export class AppModule { }
