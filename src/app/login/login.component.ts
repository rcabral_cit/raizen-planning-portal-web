import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../infrastructure/auth/auth.service';

@Component({
  selector: 'app-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
})
export class LoginComponent {

  constructor(private authService: AuthService, private router: Router) { }

  public login(): void {
    this.authService.addCSOnlineToken('teste');
    this.router.navigate(['home']);
  }

}
