import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private csonlineTokenKey = 'raizen-csconline-token';
  private userSystemTokenKey = 'raizen-usersystem-token';

  constructor(private router: Router) { }

  public addCSOnlineToken(token: string): void {
    sessionStorage.setItem(this.csonlineTokenKey, token);
  }

  public addUserSystemToken(token: string): void {
    sessionStorage.setItem(this.userSystemTokenKey, token);
  }

  public getCSOnlineToken(): string {
    return sessionStorage.getItem(this.csonlineTokenKey);
  }

  public getUserSystemToken(): string {
    return sessionStorage.getItem(this.userSystemTokenKey);
  }

  public isLoggedIn(): boolean {
    return this.getCSOnlineToken() !== null ||
      this.getUserSystemToken() !== null;
  }

  public logout(): void {
    sessionStorage.removeItem(this.csonlineTokenKey);
    sessionStorage.removeItem(this.userSystemTokenKey);
    this.router.navigate(['login']);
  }
}
