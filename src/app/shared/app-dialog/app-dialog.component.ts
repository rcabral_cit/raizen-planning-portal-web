import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-dialog-content',
  styleUrls: ['./app-dialog-content.scss'],
  templateUrl: 'app-dialog-content.html',
})
export class AppDialogContent {
  @Input()
  public title: string;

  @Input()
  public subtitle: string;

  @Input()
  public alternativeStyle: boolean;
}

@Component({
  selector: 'app-dialog',
  styleUrls: ['./app-dialog.component.scss'],
  templateUrl: './app-dialog.component.html',
})

export class AppDialogComponent {

  @Output()
  public yesOption: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  public openDialog(title, subtitle, alternativeStyle, categoryAnalytics, confirAnalytics, cancelAnalytics) {
    const dialogRef = this.dialog.open(AppDialogContent);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.subtitle = subtitle;
    dialogRef.componentInstance.alternativeStyle = alternativeStyle;
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.yesOption.emit();
      }
    });
  }
}
