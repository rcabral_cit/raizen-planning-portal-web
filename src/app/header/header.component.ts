import { Component } from '@angular/core';
import { AuthService } from '../infrastructure/auth/auth.service';

@Component({
  selector: 'header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent {

  constructor(private auth: AuthService) { }

  public isLogged(): boolean {
    return this.auth.isLoggedIn();
  }

  public logout(): void {
    this.auth.logout();
  }
}
